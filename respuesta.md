## Respuesta: Ejercicio 3. Análisis general

* ¿Cuántos paquetes componen la captura?
    * 1090 paquetes
* ¿Cuánto tiempo dura la captura?
    * 14.491079516 segundos
* ¿Qué IP tiene la máquina donde se ha efectuado la captura? ¿Se trata de una IP pública o de una IP privada? ¿Por qué lo sabes?
    * La dirección IP es 192.168.1.34. Se trata de una IP privada, ya que está dentro del rango de direcciones de la clase C.

Antes de analizar las tramas, mira las estadísticas generales que aparecen en el menú de `Statistics`. En el apartado de jerarquía de protocolos (`Protocol Hierarchy`) se puede ver el número de paquetes y el tráfico correspondiente a los distintos protocolos.

* ¿Cuáles son los tres principales protocolos de nivel de aplicación por número de paquetes?
    * Los protocolos son RTP, SIP y STUN
* ¿Qué otros protocolos podemos ver en la jerarquía de protocolos?
    * Podemos ver Ethernet, IP e ICMP
* ¿Qué protocolo de nivel de aplicación presenta más tráfico, en bytes por segundo? ¿Cuánto es ese tráfico?
    * El protocolo RTP, con 12,37 Mbytes/s

Observa por encima el flujo de tramas en el menú de `Statistics` en `IO Graphs`. La captura que estamos viendo incluye desde la inicialización (registro) de la aplicación hasta su finalización, incluyendo una llamada. 

* Filtra por `sip` para conocer cuándo se envían paquetes SIP. ¿En qué segundos tienen lugar esos envíos?
    * En los segundos 0, 3 y 14
* Y los paquetes con RTP, ¿cuándo se empiezan a enviar?
    * En el segundo 3
* Los paquetes RTP, ¿cuándo se dejan de enviar?
    * En el segundo 14
* Los paquetes RTP, ¿cada cuánto se envían?
    * Cada 10ms 

## Respuesta: Ejercicio 4. Primeras tramas

Analiza ahora las dos primeras tramas de la captura. Cuando se pregunta por máquinas, llama "Linphone" a la máquina donde está funcionando el cliente SIP, y "Servidor" a la máquina que proporciona el servicio SIP.

* ¿De qué protocolo de nivel de aplicación son?
  * Protocolo SIP
* ¿Cuál es la dirección IP de la máquina "Linphone"?
  * 192.168.1.34
* ¿Cuál es la dirección IP de la máquina "Servidor"?
  * 212.79.111.155
* ¿En qué máquina está el UA (user agent)?
en la 192.168.1.34
* ¿Entre qué máquinas se envía cada trama?
  * La trama 1: entre 192.168.1.34 (Linphone) y 212.79.111.155 (Servidor) y la trama 2: entre 212.79.111.155 (Servidor) y 192.168.1.34 (Linphone)
* ¿Que ha ocurrido para que se envíe la primera trama?
  * Se envía una petición register 
* ¿Qué ha ocurrido para que se envíe la segunda trama?
  * Se le deniega la petición al cliente, ya que no ha enviado correctamente sus datos de autenticación. 
  
  Ahora, veamos las dos tramas siguientes.

* ¿De qué protocolo de nivel de aplicación son?
  * Protocolo SIP
* ¿Entre qué máquinas se envía cada trama?
  * La trama 3: entre 192.168.1.34 (Linphone) y 212.79.111.155 (Servidor) y la trama 4: entre 212.79.111.155 (Servidor) y 192.168.1.34 (Linphone)
* ¿Que ha ocurrido para que se envíe la primera de ellas (tercera trama en la captura)?
    * El cliente vuelve a mandar una petición register 
* ¿Qué ha ocurrido para que se envíe la segunda de ellas (cuarta trama en la captura)?
    * En este caso la petición es aceptada, ya que están los datos correctos 

Ahora, las tramas 5 y 6.

* ¿De qué protocolo de nivel de aplicación son?
  * Protocolo SIP
* ¿Entre qué máquinas se envía cada trama?
  * La trama 5: entre 192.168.1.34 (Linphone) y 212.79.111.155 (Servidor) y la trama 6: entre 212.79.111.155 (Servidor) y 192.168.1.34 (Linphone)
* ¿Que ha ocurrido para que se envíe la primera de ellas (quinta trama en la captura)?
* Se manda un mensaje INVITE para comenzar el intercambio de datos 
* ¿Qué ha ocurrido para que se envíe la segunda de ellas (sexta trama en la captura)?
    * El servidor contesta con un Ok y acepta el intercambio
* ¿Qué se indica (en líneas generales) en la parte SDP de cada una de ellas?
    * Se indican datos del cliente, así como el puerto que se va a utilizar, el tipo de datos a intercambiar, el protocolo, formato... 
Después de la trama 6, busca la primera trama SIP.

* ¿Qué trama es?
  * La trama 11, que es una respuesta ACK
* ¿De qué máquina a qué máquina va?
    * entre 192.168.1.34 (Linphone) y 212.79.111.155 (Servidor)
* ¿Para qué sirve?
    * Sirve para aceptar las condiciones del intercambio de datos 
* ¿Puedes localizar en ella qué versión de Linphone se está usando?
  * La versión 3.12.0

## Respuesta: Ejercicio 5. Tramas especiales

Las tramas 7 y 9 parecen un poco especiales:

* ¿De que protocolos son (indica todos los protocolos relevantes por encima de IP).
  * Protocolos UDP y STUN
* De qué máquina a qué máquina van?
  * entre 192.168.1.34 (Linphone) y 212.79.111.155 (Servidor)
* ¿Para qué crees que sirven?
  * STUN sirve para determinar las direcciones ip y los puertos 
  
## Respuesta: Ejercicio 6. Registro

Repasemos ahora qué está ocurriendo (en lo que al protocolo SIP se refiere) en las primeras tramas SIP que hemos visto ya:

* ¿Qué dirección IP tiene el servidor que actúa como registrar SIP? ¿Por qué?
  * 212.79.111.155
* ¿A qué puerto (del servidor Registrar) se envían los paquetes SIP?
  * al 5060
* ¿Qué método SIP utiliza el UA para registrarse?
  * register
* ¿Qué diferencia fundamental ves entre la primera línea del paquete SIP que envía Linphone para registrar a un usuario, y el que hemos estado enviando en la práctica 4?

* ¿Por qué tenemos dos paquetes `REGISTER` en la traza?
  * Se ha tenido que mandar por segunda vez porque el primero fue denegado 
* ¿Por qué la cabecera `Contact` es diferente en los paquetes 1 y 3? ¿Cuál de las dos cabeceras es más "correcta", si nuestro interés es que el UA sea localizable?
  * Porque en la primera no se mandan los datos correctamente, por lo tanto la segunda es más correcta
* ¿Qué tiempo de validez se está dando para el registro que pretenden realizar los paquetes 1 y 3?
  * 3600 seg 
  
## Respuesta: Ejercicio 7. Indicación de comienzo de conversación

* Además de REGISTER, ¿podrías decir qué instrucciones SIP entiende el UA?
  * invite, ack y bye 
* ¿En qué paquete el UA indica que quiere comenzar a intercambiar datos de audio con otro? ¿Qué método SIP utiliza para ello?
  * En el paquete 5, INVITE 
* ¿Con qué dirección quiere intercambiar datos de audio el UA?
  * 212.79.111.155 (Servidor)
* ¿Qué protocolo (formato) está usando para indicar cómo quiere que sea la conversación?
  * SDP
* En la inidicacion de cómo quiere que sea la conversación, puede verse un campo `m` con un valor que empieza por `audio 7078`. ¿Qué indica el `7078`? ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama? ¿Qué paquetes son esos?
  * Indica el puerto al que el cliente va a mandar los próximos paquetes de audio 
* En la respuesta a esta indicacion vemos un campo `m` con un valor que empieza por `audio 27138`. ¿Qué indica el `27138`?  ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama? ¿Qué paquetes son esos?
  * Indica el puerto al que el servidor va a mandar los próximos paquetes de audio 
* ¿Para qué sirve el paquete 11 de la trama?
  * Es un ACK para validar las condiciones y empezar la comunicacion 

## Respuesta: Ejercicio 8. Primeros paquetes RTP

Vamos ahora a centrarnos en los paquetes RTP. Empecemos por el paquete 12 de la traza:

* ¿De qué máquina a qué máquina va?
  * entre 192.168.1.34 (Linphone) y 212.79.111.155 (Servidor)
* ¿Qué tipo de datos transporta?
  * audio
* ¿Qué tamaño tiene?
  * 214 bytes
* ¿Cuántos bits van en la "carga de pago" (payload)
  * 1376 bits
* ¿Se utilizan bits de padding?
  * no (false)
* ¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?
  * 20 ms aproximadamente
* ¿Cuántos bits/segundo se envían?
  * 99k bits/s.
  
Y ahora, las mismas preguntas para el paquete 14 de la traza:

* ¿De qué máquina a qué máquina va?
  * entre 212.79.111.155 (Servidor) y 192.168.1.34 (Linphone) 
* ¿Qué tipo de datos transporta?
  * audio
* ¿Qué tamaño tiene?
  * 214 bytes
* ¿Cuántos bits van en la "carga de pago" (payload)
  * 1376 bits
* ¿Se utilizan bits de padding?
  * no (false)
* ¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?
  * 20 ms aproximadamente
* ¿Cuántos bits/segundo se envían?
  * 99k bits/s.


## Respuesta: Ejercicio 9. Flujos RTP

Vamos a ver más a fondo el intercambio RTP. Busca en el menú `Telephony` la opción `RTP`. Empecemos mirando los flujos RTP.

* ¿Cuántos flujos hay? ¿por qué?
  * 2, entre cliente-servidor y entre servidor-cliente 
* ¿Cuántos paquetes se pierden?
  * Ningún paquete 
* Para el flujo desde LinPhone hacia Servidor: ¿cuál es el valor máximo del delta?
  * 30.834 ms
* Para el flujo desde Servidor hacia LinPhone: ¿cuál es el valor máximo del delta?
  * 59.586 ms 
* ¿Qué es lo que significa el valor de delta?
  * Es el incremento de tiempo desde el paquete anterior
* ¿En qué flujo son mayores los valores de jitter (medio y máximo)?
  * En el flujo cliente-servidor
* ¿Cuáles son esos valores?
  * El máximo 8.45ms y el medio 4.15ms
* ¿Qué significan esos valores?
  * Es la variación en el retraso de paquetes. Cuanto mayor sea la variación, mayor es el jitter
* Dados los valores de jitter que ves, ¿crees que se podría mantener una conversación de calidad?
  * Sí, ya que están por debajo de los 20 ms 
Vamos a ver ahora los valores de un paquete concreto, el paquete 17. Vamos a analizarlo en opción `Stream Analysis`:

* ¿Cuánto valen el delta y el jitter para ese paquete?
  * La delta 20.58 ms y el jitter 0.10 ms
* ¿Podemos saber si hay algún paquete de este flujo, anterior a este, que aún no ha llegado?
  * Sí, mediante el número de secuencia. En este caso han llegado todos
* El "skew" es negativo, ¿qué quiere decir eso?
  * Que el paquete se ha retrasado, en este caso 0.56
Si sigues el jitter, ves que en el paquete 78 llega a ser de 5.52:

* ¿A qué se debe esa subida desde el 0.57 que tenía el paquete 53?
  * Debido a la variación en el retraso de paquetes 
En el panel `Stream Analysis` puedes hacer `play` sobre los streams:

* ¿Qué se oye al pulsar `play`?
  * Música y una conversación de fondo
* ¿Qué se oye si seleccionas un `Jitter Buffer` de 1, y pulsas `play`?
  * Se escucha la canción entrecortada 
* ¿A qué se debe la diferencia?
  * Al retraso de los paquetes 

## Respuesta: Ejercicio 10. Llamadas VoIP

Podemos ver ahora la traza, analizándola como una llamada VoIP completa. Para ello, en el menú `Telephony` seleccina el menú `VoIP calls`, y selecciona la llamada hasta que te salga el panel correspondiente:

* ¿Cuánto dura la llamada?
  * 10 segundos 

Ahora, pulsa sobre `Flow Sequence`:

* Guarda el diagrama en un fichero (formato PNG) con el nombre `diagrama.png`.
* ¿En qué segundo se realiza el `INVITE` que inicia la conversación?
  * En el segundo 3.877
* ¿En qué segundo se recibe el último OK que marca su final?
  * En el segundo 14.49
  
Ahora, pulsa sobre `Play Sterams`, y volvemos a ver el panel para ver la transmisión de datos de la llamada (protocolo RTP):

* ¿Cuáles son las SSRC que intervienen?
  * 0x761f98b2 y 0xeeccf15f
* ¿Cuántos paquetes se envían desde LinPhone hasta Servidor?
  *  524 paquetes 
* ¿Cuántos en el sentido contrario?
  * 525 paquetes 
* ¿Cuál es la frecuencia de muestreo del audio?
  * 8000 Hz
* ¿Qué formato se usa para los paquetes de audio?
  * g711U

## Respuesta: Ejercicio 11. Captura de una llamada VoIP

Dirígete a la [web de LinPhone](https://www.linphone.org/freesip/home) con el navegador y créate una cuenta SIP.  Recibirás un correo electrónico de confirmación en la dirección que has indicado al registrarte (mira en tu carpeta de spam si no es así).
  
Lanza LinPhone, y configúralo con los datos de la cuenta que te acabas de crear. Para ello, puedes ir al menú `Ayuda` y seleccionar `Asistente de Configuración de Cuenta`. Al terminar, cierra completamente LinPhone.

* Captura una llamada VoIP con el identificador SIP `sip:music@sip.iptel.org`, de unos 15 segundos de duración. Recuerda que has de comenzar a capturar tramas antes de arrancar LinPhone para ver todo el proceso. Guarda la captura en el fichero `linphone-music.pcapng`. Procura que en la captura haya sólo paquetes entre la máquina donde has hecho la captura y has ejecutado LinPhone, y la máquina o máquinas que han intervenido en los intercambios SIP y RTP con ella.
* ¿Cuántos flujos tiene esta captura?
    * 2 flujos 
* ¿Cuánto es el valor máximo del delta y los valores medios y máximo del jitter de cada uno de los flujos?
  * Entre 212.79.111.155 y 212.128.255.211 la delta es 27.488 ms y el jitter 1.240. En el otro sentido la delta es 42.255 ms y el jitter 7.410
  
Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.


